﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{


    [SerializeField] private int value = 1;

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.CompareTag("Player"))
        {
            coll.GetComponent<Pocket>().GetCoin(value);
            gameObject.SetActive(false);
        }
    }
}
