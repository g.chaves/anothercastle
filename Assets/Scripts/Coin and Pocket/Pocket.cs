﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocket : MonoBehaviour
{

    private int cash;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetCoin(int value)
    {
        cash += value;
        UIController.Instance.UpdateCash(cash.ToString());
    }


}
