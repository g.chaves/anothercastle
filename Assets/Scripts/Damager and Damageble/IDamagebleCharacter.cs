﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagebleCharacter
{
    /// <summary>
    /// Interface de objetos que podem sofrer dano
    /// </summary>


    void GetDamage(float damage, Action action);
    void GetDamage(float damage);
}
