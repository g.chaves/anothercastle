﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    /// <summary>
    /// Classe que detecta colisão e gera dano ao objeto colidido
    /// </summary>

    [SerializeField] private float _basicDamage = 1;
    [SerializeField] protected LayerMask LayerMask;

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {

        if(LayerMask == (LayerMask | (1 << collision.gameObject.layer)))
            OnHit(collision.transform, _basicDamage);
    }


    protected virtual void OnHit(Transform opposer, float damage, Action action)
    {
        var enemyController = opposer.GetComponent<IDamagebleCharacter>();
        enemyController.GetDamage(damage, action);
    }


    protected virtual void OnHit(Transform opposer, float damage)
    {
        
        var enemyController = opposer.GetComponent<IDamagebleCharacter>();

        enemyController.GetDamage(damage);
    }

}
