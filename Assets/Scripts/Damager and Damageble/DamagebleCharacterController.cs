﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using UnityEngine;

public class DamagebleCharacterController : MonoBehaviour, IDamagebleCharacter
{

    /// <summary>
    /// Genrenciador de saúde
    /// </summary>

    protected CharactersAttributes Attributes;

    [SerializeField]  protected CharactersAttributes DefaultAttributes;
	
    protected void Start () {
		Attributes = Instantiate(DefaultAttributes);
    }


    public virtual void GetDamage(float damage, Action action)
    {
        Attributes.Life -= damage;
        if (Attributes.Life <= 0)
        {
            action();
            transform.position = Vector3.down*1000;
            //bug o ideal seria desabilitar, mas fazendo isso não chama OnTriggerExit2D(), causando problema na movimentação do jogador
            //gameObject.SetActive(false);
        }
    }

    public virtual void GetDamage(float damage)
    {
        Attributes.Life -= damage;

    }

    public virtual void ResetAttributes()
    {
        Attributes = Instantiate(DefaultAttributes);
    }
}
