﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagerEffects
{
    /// <summary>
    /// Interface de efeitos das espadas
    /// </summary>

    void OnEnemyDeath();
    void OnAttack();

}
