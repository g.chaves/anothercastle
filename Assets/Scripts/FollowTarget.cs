﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{


    [SerializeField] private Transform _target;

    [SerializeField] private float speed = 0.1f;

    private bool canFollow = false;
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

        if(canFollow)
		    Follow();
	}

    void Follow()
    {
        transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, speed);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.CompareTag("Player"))
        {
            canFollow = true;
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.transform.CompareTag("Player"))
        {
            canFollow = false;
        }
    }
}
