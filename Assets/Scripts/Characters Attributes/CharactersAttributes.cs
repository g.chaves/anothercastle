﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Attributes", menuName = "Game Dependencies/Characters Attributes", order = 1)]
public class CharactersAttributes : ScriptableObject
{
    public float Life;
}
