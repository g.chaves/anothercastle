﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldSword : SwordController {

    /// <summary>
    /// Especialização para GoldSword, ao matar um inimigo ela irá duplicar a quantidade de moedas de premiação
    /// </summary>

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnEnemyDeath()
    {
        base.OnEnemyDeath();
        print("gold sword");
    }
}
