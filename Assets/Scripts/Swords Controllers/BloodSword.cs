﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodSword : SwordController {


    /// <summary>
    /// Especialização para BloodSword, o dano da espada irá aumentar a medida que ataca.
    /// </summary>

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnEnemyDeath()
    {
        base.OnEnemyDeath();
        print("Blood sword");
    }

    public override void OnAttack()
    {
        base.OnAttack();
        
    }
}
