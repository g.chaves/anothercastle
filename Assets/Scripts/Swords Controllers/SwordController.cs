﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : Damager, IDamagerEffects
{

    /// <summary>
    /// Classe generica para efeitos das espadas
    /// </summary>

    [SerializeField] private SwordAttributes _swordAttributes;



    public virtual void OnEnemyDeath()
    {
        
    }

    public virtual void OnAttack()
    {
        
    }
    protected override void OnTriggerEnter2D(Collider2D coll)
    {
        OnHit(coll.transform, _swordAttributes.Damage, OnEnemyDeath);
    }



    protected override void OnHit(Transform opposer, float damage, Action action)
    {
        if (LayerMask == (LayerMask | (1 << opposer.gameObject.layer)))
        {
            OnAttack();
            base.OnHit(opposer, _swordAttributes.Damage, OnEnemyDeath);
        }
    }
}
