﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordPlaceHolderController : MonoBehaviour
{
    /// <summary>
    /// Controllador da espada do player, delimita o local da espada, animações e ações da espada
    /// </summary>

    private Transform _swordView;
    private Animator _swordAnimator;

    private IDamagerEffects _swordController;
	
	void Start ()
	{
	    _swordView = transform.GetChild(0);

        _swordAnimator = _swordView.GetComponent<Animator>();
	    _swordController = _swordView.GetComponent<IDamagerEffects>();
	}
	


    public void ChangeDirection()
    {
        var newPos = transform.localPosition;
        newPos.x *= -1;
        transform.localPosition = newPos;
        var newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
    }

    public void Attack()
    {
        _swordAnimator.SetTrigger("Attack");
    }

    public void ChangeSword(string swordLocation)
    {
        var swordPosition = _swordView.position;
        var tempSword = Instantiate(Resources.Load(swordLocation) as GameObject, swordPosition, Quaternion.identity, transform ).transform;
        Destroy(_swordView.gameObject);
        _swordView = tempSword;
        _swordController = _swordView.GetComponent<IDamagerEffects>();
        _swordAnimator = _swordView.GetComponent<Animator>();
    }
}
