﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class DialogEventSystem : MonoBehaviour {


    void ShowTutorial(bool op)
    {

        if (!op)
        {
            UIController.Instance.ShowDialogScreen(false);
            
        }
        UIController.Instance.RemoveAnswers();
    }

    public void ShowTutorialAnswers()
    {
        UIController.Instance.AddAnswer("yes", ShowTutorial, true);
        UIController.Instance.AddAnswer("no", ShowTutorial, false);

    }

    public void RemoveAllAnswers()
    {
        UIController.Instance.RemoveAnswers();
    }

    public void ShowCompleteGameScreen()
    {
        UIController.Instance.ShowCompleteGameScreen();
    }
	
}
