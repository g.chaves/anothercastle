﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Dialogue
{


    public Sentence[] sentences;


}

[System.Serializable]
public class Sentence
{
    public string name;
    [TextArea(3, 10)]
    public string text;
    public UnityEvent action;

    public Sentence(string name, string text, UnityEvent action)
    {
        this.name = name;
        this.text = text;
        this.action = action;
    }
}
