﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{


    private Queue<Sentence> _sentences;

	// Use this for initialization
	void Start () {
		_sentences = new Queue<Sentence>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetButtonDown("Fire1"))
	    {
	        DisplayNextSentence();
	    }
	}

    public void StartDialogue(string name, Dialogue dialogue)
    {

        UIController.Instance.ShowDialogScreen();


        _sentences.Clear();
        foreach (var sentence in dialogue.sentences)
        {
            _sentences.Enqueue(new Sentence(sentence.name, sentence.text, sentence.action));
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (_sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        Sentence sentence = _sentences.Dequeue();

        UIController.Instance.UpdateDialogue(sentence.name, sentence.text);
        sentence.action.Invoke();
    }

    private void EndDialogue()
    {

        UIController.Instance.ShowDialogScreen(false);

    }
}

