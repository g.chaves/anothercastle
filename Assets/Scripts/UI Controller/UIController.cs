﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIController : GenericSingletonClass<UIController>
{

    [SerializeField] private Text _lifeText, _cashText, _dialogueName, _dialogueText;
    [SerializeField] private GameObject _inventoryPanel, _endGamePanel, _mainMenuPanel,
        _inGameMenuPanel, _dialoguePanel, _answersContainer, _defaultAnswer, _completedGamePanel;



    public void UpdateLife(string life)
    {
        _lifeText.text = life;
    }
    public void UpdateCash(string life)
    {
        _cashText.text = life;
    }

    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            _inventoryPanel.SetActive(true);
            Time.timeScale = 0;

        }
        else
        {
            _inventoryPanel.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void ShowEndGameScreen(bool op = true)
    {
        _endGamePanel.SetActive(op);
    }

    public void ShowMainMenuScreen(bool op)
    {
        _mainMenuPanel.SetActive(op);
    }
    public void ShowInGameMenuScreen(bool op)
    {
        _inGameMenuPanel.SetActive(op);
    }

    public void ShowDialogScreen(bool op = true)
    {
        _dialoguePanel.SetActive(op);
        RemoveAnswers();
    }
    public void UpdateDialogue(string name, string text)
    {
        _dialogueName.text = name;
        _dialogueText.text = text;
    }


    public void AddAnswer(string text, UnityAction action)
    {
        var tempAnswer = Instantiate(_defaultAnswer, _answersContainer.transform);
        tempAnswer.GetComponent<Text>().text = text;
        tempAnswer.GetComponent<Button>().onClick.AddListener(action);
    }

    internal void AddAnswer(string text, Action<bool> action, bool op)
    {
        var tempAnswer = Instantiate(_defaultAnswer, _answersContainer.transform);
        tempAnswer.GetComponent<Text>().text = text;

        tempAnswer.GetComponent<Button>().onClick.AddListener(
            delegate { action(op); }
        );
    }

    public void RemoveAnswers()
    {
        foreach (Transform child in _answersContainer.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void ShowCompleteGameScreen(bool op = true)
    {
        _completedGamePanel.SetActive(op);
    }
}
