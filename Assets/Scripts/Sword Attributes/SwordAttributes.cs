﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sword Attributes", menuName = "Game Dependencies/Sword Attributes", order = 1)]
public class SwordAttributes : ScriptableObject
{
    public float Damage;
}
