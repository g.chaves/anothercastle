﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class GameManager : GenericSingletonClass<GameManager>
{


    [SerializeField] private GameObject Player;
	// Use this for initialization
	void Start () {
	    Time.timeScale = 0;

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void StartGame(bool op)
    {
        UIController.Instance.ShowMainMenuScreen(!op);
        UIController.Instance.ShowInGameMenuScreen(op);
        Player.SetActive(op);
        Time.timeScale = op? 1 : 0;
    }

    public void RestartGame()
    {
        Player.transform.position = Vector3.zero;
        print(Player.transform.position);
        UIController.Instance.ShowEndGameScreen(false);
        UIController.Instance.ShowCompleteGameScreen(false);
        Player.GetComponent<PlayerHealthController>().ResetAttributes();
        StartGame(true);

    }

    public void EndGame()
    {
        UIController.Instance.ShowEndGameScreen();
    }
}
