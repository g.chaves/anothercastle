﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetection : MonoBehaviour
{
    /// <summary>
    /// Esta classe serve para avisar ao PlayerController se ele esta tocando paredes e parar de andar,
    /// Evitando que ele continue adcionando força para movimentar.
    /// A tag serve para identificar qual o objeto que reage ao trigger
    /// A messagem é o lado do player que esta tocando numa parede.

    /// </summary>

    private PlayerController _playerController;
    public string Tag;
    public LayerMask CollisionLayerMask;
    public int Message;

	void Start ()
	{
	    _playerController = transform.GetComponentInParent<PlayerController>();
	}
	

	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (Tag == "Floor")
        {
            _playerController.EnterOnFloor();

        }

        else if(CollisionLayerMask == (CollisionLayerMask | (1 << col.gameObject.layer)))
        {
            _playerController.EnterOnWall(Message);
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (Tag == "Floor")
        {
            _playerController.ExitOnFloor();
        }
        else if (CollisionLayerMask == (CollisionLayerMask | (1 << col.gameObject.layer)))
        {
            _playerController.ExitOnWall(Message);
        }

    }

}
