﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : DamagebleCharacterController
{
    /// <summary>
    /// Classe responsável pela saúde do player
    /// </summary>



    void Start()
    {
        base.Start();
        UIController.Instance.UpdateLife(Attributes.Life.ToString());

    }
    public override void GetDamage(float damage, Action action)
    {

        base.GetDamage(damage, action);
        UIController.Instance.UpdateLife(Attributes.Life.ToString());
    }
    public override void GetDamage(float damage)
    {
        base.GetDamage(damage);
        if (Attributes.Life <= 0)
        {
            GameManager.Instance.EndGame();
            UIController.Instance.UpdateLife("0");

            return;
        }
        UIController.Instance.UpdateLife(Attributes.Life.ToString());

    }

    public override void ResetAttributes()
    {
        base.ResetAttributes();
        UIController.Instance.UpdateLife(Attributes.Life.ToString());

    }
}
