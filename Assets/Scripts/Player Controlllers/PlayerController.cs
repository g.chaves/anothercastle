﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Classe responsável pela input de movimentação, animãção e colisões.
    /// </summary>

    [SerializeField]
    private float _maxSpeed, _maxJumpForce, _maxDashForce, _maxSmashForce, _jumpTime = 0.2f;

    [SerializeField]
    private int _maxNumberJump;

    private int _jumps;


    private bool _canDash = true, _onFloor = false, _jumpButtonPressed, _canSmash, _jumping, _controllerEnabled = true, _isDashing = false;
    private bool[] _touchingWalls = { false, false };

    [SerializeField] private SwordPlaceHolderController _swordPlaceHolder;

    private Vector2 _jumpVector, _forward;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    private Rigidbody2D _rb;
    
    
    void Start ()
	{
	    _rb = GetComponent<Rigidbody2D>();
	    _spriteRenderer = GetComponent<SpriteRenderer>();
	    _animator = GetComponent<Animator>();
	    _jumpVector = Vector2.up * _maxJumpForce;
        _forward = Vector2.right;
	}

	void Update ()
    {
	    CharacterControll();
    }


    void OnEnable()
    {
        _canSmash = true;
        _jumps = 0;
    }

    public void CharacterControll()
    {
        if (!_controllerEnabled) return;
        _animator.SetBool("Jump", _jumping);

        if (Input.GetButtonDown("Fire1"))
        {
            _animator.SetTrigger("Attack");
            _swordPlaceHolder.Attack();
        }

        if (Input.GetButtonDown("Jump"))
        {
            _jumpButtonPressed = true;
            
            Jump();

        }
        if (Input.GetButtonUp("Jump"))
        {
            _jumpButtonPressed = false;
        }

        if (Input.GetButtonDown("Dash"))
        {

            Dash(_forward, _maxDashForce);
        }

        if (Input.GetAxisRaw("Horizontal") != 0 && !_isDashing)
        {
           
            _forward = new Vector2(Input.GetAxisRaw("Horizontal"), 0);
            MoveHorizontally(_forward, _maxSpeed);
            _animator.SetBool("Walk", true);

        }
        else
        {
            _animator.SetBool("Walk", false);
        }

    }


    public void Jump(float maxJumpForce)
    {
        if (_jumps >= _maxNumberJump)
            return;

        StartCoroutine(JumpRoutine());
        _jumps++;
    }

    public void Jump()
    {
        Jump(_maxJumpForce);
    }
    public void FasterJump()
    {
        Jump(_maxJumpForce * 1.2f);
    }

    void MoveHorizontally(Vector2 direction, float maxSpeed)
    {
        if (direction.x > 0 && _touchingWalls[1]) return;
        if (direction.x < 0 && _touchingWalls[0]) return;

        _rb.velocity = new Vector2(direction.x * maxSpeed, _rb.velocity.y);

        bool flipSprite = (_spriteRenderer.flipX ? (direction.x > 0.01f) : (direction.x < -0.01f));
        if (flipSprite)
        {
            _spriteRenderer.flipX = !_spriteRenderer.flipX;
            _swordPlaceHolder.ChangeDirection();
        }
    }

    void MoveHorizontally(Vector2 direction)
    {
        MoveHorizontally(direction, _maxSpeed);
    }

    void Dash(Vector2 direction, float force )
    {

        _isDashing = true;
        _rb.AddForce(direction * force, ForceMode2D.Impulse);
        StartCoroutine(DelayedAction(0.2f, () => { _isDashing = false; }));

    }

    IEnumerator DelayedAction(float time, Action action)
    {
        yield return new WaitForSeconds(time);

        action();
    }

    void Dash(Vector2 direction, float force, ref bool canDash)
    {
        if (!canDash) return;
        Dash(direction, force);
        canDash = false;
    }

    void Smash()
    {
        if (!_canSmash) return;
        Dash(Vector2.down, _maxSmashForce);
        _canSmash = false;
    }
    private void ResetJumps()
    {
        if (_onFloor)
            _jumps = 0;
        else
        {
        _jumps = _maxNumberJump - 1;

        }
    }




    void Reset(ref bool option)
    {
        option = true;
    }




    void OnCollisionEnter2D(Collision2D coll)
    {



        if (coll.transform.CompareTag("Enemy") && _controllerEnabled)
        {
            Death();
        }
    }

    void Death()
    {
        
    }




    public void EnterOnFloor()
    {
        _onFloor = true;
        ResetJumps();
        Reset(ref _canSmash);
        Reset(ref _canDash);
        _jumping = false;
    }

    public void ExitOnFloor()
    {
            _onFloor = false;

    }


    public void EnterOnWall(int mes)
    {
        _touchingWalls[mes < 0 ? 0 : 1] = true;

    }

    public void ExitOnWall(int mes)
    {
        _touchingWalls[mes < 0 ? 0 : 1] = false;


    }


    public void GoThrough(bool op, Collider2D coll)
    {

        Physics2D.IgnoreCollision(coll, GetComponent<Collider2D>(), op);
    }



    IEnumerator JumpRoutine()
    {

        float timer = 0;
        _jumping = true;
        while (_jumpButtonPressed && timer < _jumpTime)
        {
            

            //Calcula o quão longe o pulo vai em porcentagem
            //Aplica a força a cada frame

            float proportionCompleted =  (timer / (_jumpTime ));
            Vector2 thisFrameJumpVector = Vector2.Lerp(_jumpVector, Vector2.zero, proportionCompleted);
            _rb.AddForce(thisFrameJumpVector);
            timer += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

    }


}
