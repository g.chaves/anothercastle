# Another Castle

A 8-Bit game to save another princess or king.

## Story Line

The king is gone, you have to rescue him.
Open the path with your sword!

## Skills

### Programming

* Most part of the code was made by scratch. (Except: Cinemachine and other unity packages)

### Design

* Hand made Characters made with Photoshop
* Background and tilemap from other sources

### Music

* https://incompetech.com/music/royalty-free/music.html

## Challenges

* This was the first time I coded a dialogue system, it was a lot of fun.
* in fact, it was a lot of fun to design the whole game!

## Authors

* Gustavo Chaves
* [@gchavesl](https://twitter.com/gchavesl)


